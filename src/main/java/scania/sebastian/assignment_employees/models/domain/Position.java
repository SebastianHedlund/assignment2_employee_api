package scania.sebastian.assignment_employees.models.domain;

public enum Position {
    TESTER,
    MANAGER,
    DEVELOPER
}
