package scania.sebastian.assignment_employees.models.domain;

public enum EmploymentHours {
    FULLTIME,
    PARTTIME
}
