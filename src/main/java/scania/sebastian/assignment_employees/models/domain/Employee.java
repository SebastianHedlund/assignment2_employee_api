package scania.sebastian.assignment_employees.models.domain;

import java.time.LocalDate;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private long employeeCode;
    private LocalDate dateHired;
    private Position position;
    private EmploymentHours employmentHours;
    private ArrayList<String> devices;

}
