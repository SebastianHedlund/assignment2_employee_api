package scania.sebastian.assignment_employees.models.maps;

import org.springframework.stereotype.Component;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.TotalEmployeeDevicesDto;

@Component
public class TotalDevicesDtoMapper {

    public TotalEmployeeDevicesDto mapTotalEmployeeDevicesDto(Employee employee){
        return new TotalEmployeeDevicesDto(
                employee.getFirstName() + " " + employee.getLastName(),
                employee.getDevices().size());
    }
}