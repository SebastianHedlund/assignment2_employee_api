package scania.sebastian.assignment_employees.models.maps;

import org.springframework.stereotype.Component;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.FullTimeEmployeeBirthdayDto;

@Component
public class FullTimeEmployeeBirthdayDtoMapper {

    public FullTimeEmployeeBirthdayDto mapFullTimeEmployeeDto(Employee employee, int daysUntilBirthday){
        return new FullTimeEmployeeBirthdayDto(
                employee.getFirstName() + " " + employee.getLastName(),
                employee.getDateOfBirth(),
                daysUntilBirthday);
    }
}
