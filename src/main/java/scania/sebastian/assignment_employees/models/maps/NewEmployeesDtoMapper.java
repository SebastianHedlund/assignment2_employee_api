package scania.sebastian.assignment_employees.models.maps;

import org.springframework.stereotype.Component;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.NewEmployeesDto;

@Component
public class NewEmployeesDtoMapper {

    public NewEmployeesDto mapNewEmployeesDto(Employee employee){
        return new NewEmployeesDto(employee.getEmployeeCode(),
                employee.getFirstName() + " " + employee.getLastName());
    }
}
