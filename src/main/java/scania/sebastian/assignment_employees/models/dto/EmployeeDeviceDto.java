package scania.sebastian.assignment_employees.models.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class EmployeeDeviceDto {
    private int id;
    private List<String> devices;
}
