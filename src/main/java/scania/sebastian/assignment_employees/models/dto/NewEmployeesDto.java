package scania.sebastian.assignment_employees.models.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
@Setter
public class NewEmployeesDto {
    private long employeeCode;
    private String fullName;
}
