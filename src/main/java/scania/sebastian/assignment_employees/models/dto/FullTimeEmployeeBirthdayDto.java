package scania.sebastian.assignment_employees.models.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
@Setter
public class FullTimeEmployeeBirthdayDto {
    private String fullName;
    private LocalDate dateOfBirth;
    private int daysUntilBirthday;
}
