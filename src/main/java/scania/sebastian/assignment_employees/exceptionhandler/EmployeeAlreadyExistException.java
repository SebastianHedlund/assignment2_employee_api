package scania.sebastian.assignment_employees.exceptionhandler;

public class EmployeeAlreadyExistException extends EmployeeException{
    public EmployeeAlreadyExistException(String message) {
        super(message);
    }
}
