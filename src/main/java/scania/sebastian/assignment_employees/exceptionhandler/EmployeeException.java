package scania.sebastian.assignment_employees.exceptionhandler;

public class EmployeeException extends Exception{
    public EmployeeException(String message) {
        super(message);
    }
}
