package scania.sebastian.assignment_employees.exceptionhandler;

public class EmployeeDoesNotExistException extends EmployeeException {
    public EmployeeDoesNotExistException(String message) {
        super(message);
    }
}
