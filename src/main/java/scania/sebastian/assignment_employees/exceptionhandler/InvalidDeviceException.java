package scania.sebastian.assignment_employees.exceptionhandler;

public class InvalidDeviceException extends EmployeeException{
    public InvalidDeviceException(String message) {
        super(message);
    }
}
