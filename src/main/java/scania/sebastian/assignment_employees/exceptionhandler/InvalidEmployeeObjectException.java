package scania.sebastian.assignment_employees.exceptionhandler;

public class InvalidEmployeeObjectException extends EmployeeException{
    public InvalidEmployeeObjectException(String message) {
        super(message);
    }
}
