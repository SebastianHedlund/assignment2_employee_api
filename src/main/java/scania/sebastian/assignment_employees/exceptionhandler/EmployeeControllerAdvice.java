package scania.sebastian.assignment_employees.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class EmployeeControllerAdvice {

    @ExceptionHandler(InvalidEmployeeObjectException.class)
    protected ResponseEntity<String> handleInvalidEmployeeObject(InvalidEmployeeObjectException invalidEmployeeObjectException){
        return new ResponseEntity<>(invalidEmployeeObjectException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmployeeDoesNotExistException.class)
    protected ResponseEntity<String> handleEmployeeDoesNotExist(EmployeeDoesNotExistException employeeDoesNotExistException){
        return new ResponseEntity<>(employeeDoesNotExistException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EmployeeAlreadyExistException.class)
    protected ResponseEntity<String> handleEmployeeAlreadyExist(EmployeeAlreadyExistException employeeAlreadyExistException){
        return new ResponseEntity<>(employeeAlreadyExistException.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(InvalidDeviceException.class)
    protected ResponseEntity<String> handleInvalidDevice(InvalidDeviceException invalidDeviceException){
        return new ResponseEntity<>(invalidDeviceException.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
