package scania.sebastian.assignment_employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentEmployeesApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentEmployeesApplication.class, args);
    }

}
