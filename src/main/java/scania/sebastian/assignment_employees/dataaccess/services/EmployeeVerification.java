package scania.sebastian.assignment_employees.dataaccess.services;

import org.springframework.stereotype.Component;
import scania.sebastian.assignment_employees.exceptionhandler.*;
import scania.sebastian.assignment_employees.models.domain.Employee;

import java.util.HashMap;
import java.util.List;

@Component
public class EmployeeVerification {

    public void verifyEmployeeId(int employeeId, int id) throws InvalidEmployeeObjectException {
        if(id != employeeId)
            throw new InvalidEmployeeObjectException("The provided employee ID does not match the objects employee ID.");
    }

    public void verifyEmployeeInDatabase(int id, HashMap<Integer, Employee> employees) throws EmployeeDoesNotExistException {
        if (!employees.containsKey(id))
            throw new EmployeeDoesNotExistException("Employee with provided ID does not exist in database.");
    }

    public void verifyEmployeeNotInDatabase(int id, HashMap<Integer, Employee> employees) throws EmployeeAlreadyExistException {
        if (employees.containsKey(id))
            throw new EmployeeAlreadyExistException("Employee with provided ID already exist in database.");
    }

    public void verifyEmployeeObject(Employee employee) throws EmployeeException {
        if(employee.getFirstName() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, firstName can not be null.");
        if(employee.getLastName() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, lastName can not be null.");
        if(employee.getDateOfBirth() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, dateOfBirth can not be null.");
        if(employee.getEmployeeCode() == 0)
            throw new InvalidEmployeeObjectException("Invalid Employee object, employeeCode can not be 0.");
        if(employee.getDateHired() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, dateHired can not be null.");
        if(employee.getPosition() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, position can not be null.");
        if(employee.getEmploymentHours() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, employmentHours can not be null.");
        if(employee.getDevices() == null)
            throw new InvalidEmployeeObjectException("Invalid Employee object, devices can not be null.");

        //EmployeeCode is part of the Employee object and need to be verified as well.
        verifyEmployeeCode(employee.getEmployeeCode());
    }

    public void verifyDeviceNotNull(String device) throws InvalidDeviceException {
        if (device == null)
            throw new InvalidDeviceException("The entered device can't be null.");
    }

    public void verifyDevicesNotNull(List<String> devices) throws InvalidDeviceException {
        if (devices == null)
            throw new InvalidDeviceException("Invalid devices object, devices can't be null.");
    }

    public void verifyEmployeeCode(long employeeCode) throws InvalidEmployeeObjectException {
        if(!validateEmployeeCodeLength(employeeCode))
            throw new InvalidEmployeeObjectException("The provided employee code must be exactly 10 digits long.");
        if(!validateEmployeeCodeLuhn(employeeCode))
            throw new InvalidEmployeeObjectException("The provided employee code is not a valid Luhn algorithm code.");
    }

    public boolean devicesEmpty(List<String> devices) {
        return devices.size() == 0;
    }

    public void validateEmployeeCodeIsUnique(int id, HashMap<Integer, Employee> employees, long employeeCode) throws EmployeeAlreadyExistException {
        for (Employee employee: employees.values()) {
            if (employee.getEmployeeCode() == employeeCode && id != employee.getId())
                throw new EmployeeAlreadyExistException("Employee with provided employee code already exists");
        }
    }

    public boolean validateEmployeeCodeLength(long employeeCode){
        return String.valueOf(employeeCode).length() == 10;
    }

    public boolean validateEmployeeCodeLuhn(long employeeCode){
        int secondNumber = 0;
        int sum = 0;

        while(employeeCode > 0){
            int lastDigit = (int) (employeeCode % 10);
            employeeCode = employeeCode / 10;
            secondNumber++;

            //Multiply every second number from last by 2
            if (secondNumber % 2 == 0)
                lastDigit *= 2;

            //calculate sum of digits in lastDigit in case they are 10 or above after previous step
            if (lastDigit != 0 && lastDigit % 9 == 0)
                lastDigit = 9;
            else
                lastDigit = lastDigit % 9;

            sum += lastDigit;
        }

        return sum % 10 == 0;
    }
}
