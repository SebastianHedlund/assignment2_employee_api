package scania.sebastian.assignment_employees.dataaccess.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeRepository;
import scania.sebastian.assignment_employees.dataaccess.repositories.WorkersRepository;
import scania.sebastian.assignment_employees.exceptionhandler.EmployeeException;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.domain.EmploymentHours;
import scania.sebastian.assignment_employees.models.dto.EmployeeDeviceDto;
import scania.sebastian.assignment_employees.models.dto.FullTimeEmployeeBirthdayDto;
import scania.sebastian.assignment_employees.models.dto.NewEmployeesDto;
import scania.sebastian.assignment_employees.models.dto.TotalEmployeeDevicesDto;
import scania.sebastian.assignment_employees.models.maps.FullTimeEmployeeBirthdayDtoMapper;
import scania.sebastian.assignment_employees.models.maps.NewEmployeesDtoMapper;
import scania.sebastian.assignment_employees.models.maps.TotalDevicesDtoMapper;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService implements WorkersService{

    private EmployeeRepository employeeRepository;
    private EmployeeVerification employeeVerification;
    private FullTimeEmployeeBirthdayDtoMapper fullTimeEmployeeBirthdayDtoMapper;
    private NewEmployeesDtoMapper newEmployeesDtoMapper;
    private TotalDevicesDtoMapper totalDevicesDtoMapper;


    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository,
                           EmployeeVerification employeeVerification,
                           FullTimeEmployeeBirthdayDtoMapper fullTimeEmployeeBirthdayDtoMapper,
                           NewEmployeesDtoMapper newEmployeesDtoMapper,
                           TotalDevicesDtoMapper totalDevicesDtoMapper){

        this.employeeRepository = employeeRepository;
        this.employeeVerification = employeeVerification;
        this.fullTimeEmployeeBirthdayDtoMapper = fullTimeEmployeeBirthdayDtoMapper;
        this.newEmployeesDtoMapper = newEmployeesDtoMapper;
        this.totalDevicesDtoMapper = totalDevicesDtoMapper;
    }

    @Override
    public List<Employee> getAllEmployees(){
        return employeeRepository.getAllEmployees();
    }

    @Override
    public Employee getEmployee(int id) throws EmployeeException {
        employeeVerification.verifyEmployeeInDatabase(id, employeeRepository.getEmployees());
        return employeeRepository.getEmployee(id);
    }

    @Override
    public Employee createEmployee(Employee employee, int id) throws EmployeeException {
        employeeVerification.verifyEmployeeId(employee.getId(), id);
        employeeVerification.verifyEmployeeNotInDatabase(id, employeeRepository.getEmployees());
        employeeVerification.verifyEmployeeObject(employee);
        employeeVerification.validateEmployeeCodeIsUnique(id, employeeRepository.getEmployees(), employee.getEmployeeCode());
        return employeeRepository.createEmployee(employee);
    }

    @Override
    public Employee replaceEmployee(Employee employee, int id) throws EmployeeException{
        employeeVerification.verifyEmployeeId(employee.getId(), id);
        employeeVerification.verifyEmployeeObject(employee);
        employeeVerification.validateEmployeeCodeIsUnique(id, employeeRepository.getEmployees(), employee.getEmployeeCode());
        return employeeRepository.replaceEmployee(employee);
    }

    @Override
    public Employee updateEmployee(Employee employee, int id) throws EmployeeException {
        employeeVerification.verifyEmployeeId(employee.getId(), id);
        employeeVerification.verifyEmployeeInDatabase(id, employeeRepository.getEmployees());

        //If any field in the received Employee is null, set it to equal the current Employee field.
        //This enables the user to change any amount of fields without having to write all unaffected ones in the JSON object.
        Employee currentEmployee = employeeRepository.getEmployees().get(id);
        if(employee.getFirstName() == null)
            employee.setFirstName(currentEmployee.getFirstName());
        if(employee.getLastName() == null)
            employee.setLastName(currentEmployee.getLastName());
        if(employee.getDateOfBirth() == null)
            employee.setDateOfBirth(currentEmployee.getDateOfBirth());
        if(employee.getEmployeeCode() == 0)
            employee.setEmployeeCode(currentEmployee.getEmployeeCode());
        if(employee.getDateHired() == null)
            employee.setDateHired(currentEmployee.getDateHired());
        if(employee.getPosition() == null)
            employee.setPosition(currentEmployee.getPosition());
        if(employee.getEmploymentHours() == null)
            employee.setEmploymentHours(currentEmployee.getEmploymentHours());
        if(employee.getDevices() == null)
            employee.setDevices(currentEmployee.getDevices());

        //Don't update anything if no changes are made
        if(employee.equals(employeeRepository.getEmployee(id)))
            return null;

        employeeVerification.verifyEmployeeCode(employee.getEmployeeCode());
        employeeVerification.validateEmployeeCodeIsUnique(id, employeeRepository.getEmployees(), employee.getEmployeeCode());
        return employeeRepository.updateEmployee(employee);
    }

    @Override
    public void deleteEmployee(int id) throws EmployeeException {
        employeeVerification.verifyEmployeeInDatabase(id, employeeRepository.getEmployees());
        employeeRepository.deleteEmployee(id);
    }

    @Override
    public Employee addEmployeeDevice(EmployeeDeviceDto devices, int id) throws EmployeeException {
        employeeVerification.verifyEmployeeInDatabase(id, employeeRepository.getEmployees());
        employeeVerification.verifyEmployeeId(devices.getId(), id);
        employeeVerification.verifyDevicesNotNull(devices.getDevices());

        //Don't update anything if no changes are made
        if(employeeVerification.devicesEmpty(devices.getDevices()))
            return null;

        for (String device: devices.getDevices()){
            employeeVerification.verifyDeviceNotNull(device);
            employeeRepository.addEmployeeDevice(device, id);
        }
        return employeeRepository.getEmployee(id);
    }

    @Override
    public Employee removeEmployeeDevice(EmployeeDeviceDto devices,int id) throws EmployeeException {
        employeeVerification.verifyEmployeeInDatabase(id, employeeRepository.getEmployees());
        employeeVerification.verifyEmployeeId(devices.getId(), id);
        employeeVerification.verifyDevicesNotNull(devices.getDevices());

        //Don't update anything if no changes are made
        if(employeeVerification.devicesEmpty(devices.getDevices()))
            return null;

        for (String device: devices.getDevices()){
            employeeVerification.verifyDeviceNotNull(device);
            employeeRepository.removeEmployeeDevice(device, id);
        }
        return employeeRepository.getEmployee(id);
    }

    @Override
    public WorkersRepository getWorkersRepository() {
        return employeeRepository;
    }

    @Override
    public ArrayList<TotalEmployeeDevicesDto> getTotalEmployeeDevices(){
        ArrayList<TotalEmployeeDevicesDto> deviceList = new ArrayList<>();

        for (Employee employee: employeeRepository.getEmployees().values()) {
            deviceList.add(totalDevicesDtoMapper.mapTotalEmployeeDevicesDto(employee));
        }
        return deviceList;
    }

    @Override
    public ArrayList<NewEmployeesDto> getNewEmployees(){
        ArrayList<NewEmployeesDto> newEmployeesList = new ArrayList<>();
        LocalDate twoYearsFromNow = LocalDate.now().minus(2, ChronoUnit.YEARS);

        for (Employee employee: employeeRepository.getEmployees().values()) {
            if (employee.getDateHired().compareTo(twoYearsFromNow) > 0)
                newEmployeesList.add(newEmployeesDtoMapper.mapNewEmployeesDto(employee));
        }
        return newEmployeesList;
    }

    @Override
    public ArrayList<FullTimeEmployeeBirthdayDto>getFullTimeEmployeeBirthdays(){
        ArrayList<FullTimeEmployeeBirthdayDto>  newEmployeesList = new ArrayList<>();
        LocalDate todaysDate = LocalDate.now();

        for (Employee employee: employeeRepository.getEmployees().values()) {
            if (employee.getEmploymentHours() != EmploymentHours.FULLTIME)
                continue;

            //Set employees birth year to current year for easier evaluation.
            int birthMonth = employee.getDateOfBirth().getMonthValue();
            int birthDay = employee.getDateOfBirth().getDayOfMonth();
            LocalDate employeeBirthDay = LocalDate.of(todaysDate.getYear(), birthMonth, birthDay);

            //If Employee already had his birthday this year, set birthday year to next year
            if (employeeBirthDay.compareTo(todaysDate) < 0)
                employeeBirthDay = employeeBirthDay.plus(1, ChronoUnit.YEARS);

            int daysUntilBirthDay = (int) ChronoUnit.DAYS.between(todaysDate, employeeBirthDay);
            newEmployeesList.add(fullTimeEmployeeBirthdayDtoMapper.mapFullTimeEmployeeDto(employee, daysUntilBirthDay));
        }

        return newEmployeesList;
    }
}
