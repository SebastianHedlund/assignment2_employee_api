package scania.sebastian.assignment_employees.dataaccess.services;

import scania.sebastian.assignment_employees.dataaccess.repositories.WorkersRepository;
import scania.sebastian.assignment_employees.exceptionhandler.EmployeeException;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.EmployeeDeviceDto;
import scania.sebastian.assignment_employees.models.dto.FullTimeEmployeeBirthdayDto;
import scania.sebastian.assignment_employees.models.dto.NewEmployeesDto;
import scania.sebastian.assignment_employees.models.dto.TotalEmployeeDevicesDto;

import java.util.ArrayList;
import java.util.List;

public interface WorkersService {
    List<Employee> getAllEmployees();
    Employee getEmployee(int id) throws EmployeeException;
    Employee createEmployee(Employee employee, int id) throws EmployeeException;
    Employee replaceEmployee(Employee employee, int id) throws EmployeeException;
    Employee updateEmployee(Employee employee, int id) throws EmployeeException;
    void deleteEmployee(int id) throws EmployeeException;
    Employee addEmployeeDevice(EmployeeDeviceDto devices, int id) throws EmployeeException;
    Employee removeEmployeeDevice(EmployeeDeviceDto devices,int id) throws EmployeeException;
    WorkersRepository getWorkersRepository();
    ArrayList<TotalEmployeeDevicesDto> getTotalEmployeeDevices();
    ArrayList<NewEmployeesDto> getNewEmployees();
    ArrayList<FullTimeEmployeeBirthdayDto>getFullTimeEmployeeBirthdays();
}
