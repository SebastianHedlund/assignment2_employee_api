package scania.sebastian.assignment_employees.dataaccess.repositories;

import org.springframework.stereotype.Component;
import scania.sebastian.assignment_employees.models.domain.Employee;

import java.util.HashMap;
import java.util.List;

@Component
public class EmployeeRepository implements WorkersRepository{
    private HashMap<Integer, Employee> employees = seedEmployee();

    private HashMap<Integer, Employee> seedEmployee(){

        EmployeeSeedData employeeSeedData = new EmployeeSeedData();
        HashMap<Integer, Employee> employeeHashMap = new HashMap<>();
        employeeHashMap.put(1, new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices()));
        employeeHashMap.put(2, new Employee(
                employeeSeedData.getSecondEmployeeId(),
                employeeSeedData.getSecondEmployeeFirstName(),
                employeeSeedData.getSecondEmployeeLastName(),
                employeeSeedData.getSecondEmployeeDateOfBirth(),
                employeeSeedData.getSecondEmployeeCode(),
                employeeSeedData.getSecondEmployeeDateHired(),
                employeeSeedData.getSecondEmployeePosition(),
                employeeSeedData.getSecondEmployeeWorkFullTime(),
                employeeSeedData.getSecondEmployeeDevices()));
        employeeHashMap.put(3, new Employee(
                employeeSeedData.getThirdEmployeeId(),
                employeeSeedData.getThirdEmployeeFirstName(),
                employeeSeedData.getThirdEmployeeLastName(),
                employeeSeedData.getThirdEmployeeDateOfBirth(),
                employeeSeedData.getThirdEmployeeCode(),
                employeeSeedData.getThirdEmployeeDateHired(),
                employeeSeedData.getThirdEmployeePosition(),
                employeeSeedData.getThirdEmployeeWorkFullTime(),
                employeeSeedData.getThirdEmployeeDevices()));

        return employeeHashMap;
    }

    @Override
    public List<Employee> getAllEmployees(){
        return employees.values().stream().toList();
    }

    @Override
    public Employee getEmployee(int id){
        return employees.get(id);
    }

    @Override
    public Employee createEmployee(Employee employee){
        employees.put(employee.getId(), employee);
        return employees.get(employee.getId());
    }

    @Override
    public HashMap<Integer, Employee> getEmployees() {
        return employees;
    }

    @Override
    public Employee replaceEmployee(Employee employee){
        return createEmployee(employee);
    }

    @Override
    public Employee updateEmployee(Employee employee){
        Employee currentEmployee = employees.get(employee.getId());
        currentEmployee.setFirstName(employee.getFirstName());
        currentEmployee.setLastName(employee.getLastName());
        currentEmployee.setDateOfBirth(employee.getDateOfBirth());
        currentEmployee.setEmployeeCode(employee.getEmployeeCode());
        currentEmployee.setDateHired(employee.getDateHired());
        currentEmployee.setPosition(employee.getPosition());
        currentEmployee.setEmploymentHours(employee.getEmploymentHours());
        currentEmployee.setDevices(employee.getDevices());
        return employees.get(employee.getId());
    }

    @Override
    public void deleteEmployee(int id){
        employees.remove(id);
    }

    @Override
    public Employee addEmployeeDevice(String device, int id){
        Employee employee = employees.get(id);
        employee.getDevices().add(device);
        return employees.get(id);
    }

    @Override
    public Employee removeEmployeeDevice(String device, int id){
        Employee employee = employees.get(id);
        employee.getDevices().remove(device);
        return employees.get(id);
    }
}
