package scania.sebastian.assignment_employees.dataaccess.repositories;

import scania.sebastian.assignment_employees.models.domain.Employee;

import java.util.HashMap;
import java.util.List;

public interface WorkersRepository {
    List<Employee> getAllEmployees();
    Employee getEmployee(int id);
    Employee createEmployee(Employee employee);
    HashMap<Integer, Employee> getEmployees();
    Employee replaceEmployee(Employee employee);
    Employee updateEmployee(Employee employee);
    void deleteEmployee(int id);
    Employee addEmployeeDevice(String device, int id);
    Employee removeEmployeeDevice(String device, int id);
}
