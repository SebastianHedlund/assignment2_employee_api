package scania.sebastian.assignment_employees.dataaccess.repositories;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import scania.sebastian.assignment_employees.models.domain.EmploymentHours;
import scania.sebastian.assignment_employees.models.domain.Position;

import java.time.LocalDate;
import java.util.ArrayList;

@Getter
@EqualsAndHashCode
public class EmployeeSeedData {
    private final int firstEmployeeId = 1;
    private final int secondEmployeeId = 2;
    private final int thirdEmployeeId = 3;

    private final String firstEmployeeFirstName = "John";
    private final String secondEmployeeFirstName = "Jane";
    private final String thirdEmployeeFirstName = "Sven";

    private final String firstEmployeeLastName = "Doe";
    private final String secondEmployeeLastName = "Doe";
    private final String thirdEmployeeLastName = "Svensson";

    private final LocalDate firstEmployeeDateOfBirth = LocalDate.of(1980,2,14);
    private final LocalDate secondEmployeeDateOfBirth = LocalDate.of(1984,5,17);
    private final LocalDate thirdEmployeeDateOfBirth = LocalDate.of(1987,3,22);

    private final long firstEmployeeCode = 2018269734L;
    private final long secondEmployeeCode = 1634751505L;
    private final long thirdEmployeeCode = 1507119608L;

    private final LocalDate firstEmployeeDateHired = LocalDate.of(2021,3,31);
    private final LocalDate secondEmployeeDateHired = LocalDate.of(2018,1,12);
    private final LocalDate thirdEmployeeDateHired = LocalDate.of(2019,2,15);

    private final Position firstEmployeePosition = Position.TESTER;
    private final Position secondEmployeePosition = Position.MANAGER;
    private final Position thirdEmployeePosition = Position.DEVELOPER;

    private final EmploymentHours firstEmployeeWorkFullTime = EmploymentHours.PARTTIME;
    private final EmploymentHours secondEmployeeWorkFullTime = EmploymentHours.FULLTIME;
    private final EmploymentHours thirdEmployeeWorkFullTime = EmploymentHours.FULLTIME;

    private final ArrayList<String> firstEmployeeDevices = new ArrayList<>(){
        {
            add("Dell XPS 13");
        }
    };
    private final ArrayList<String> secondEmployeeDevices = new ArrayList<>(){
        {
            add("Alienware X17");
            add("Samsung 30-inch LED Monitor");
        }
    };
    private final ArrayList<String> thirdEmployeeDevices = new ArrayList<>(){
        {
            add("Dell XPS 15");
            add("Samsung 27-inch LED Monitor");
        }
    };

}
