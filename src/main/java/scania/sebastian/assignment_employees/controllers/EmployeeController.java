package scania.sebastian.assignment_employees.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.assignment_employees.dataaccess.services.EmployeeService;
import scania.sebastian.assignment_employees.exceptionhandler.EmployeeException;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.EmployeeDeviceDto;
import scania.sebastian.assignment_employees.models.dto.FullTimeEmployeeBirthdayDto;
import scania.sebastian.assignment_employees.models.dto.NewEmployeesDto;
import scania.sebastian.assignment_employees.models.dto.TotalEmployeeDevicesDto;

import java.util.List;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {
    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public ResponseEntity<List<Employee>> getAllEmployees(){
        return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable int id) throws EmployeeException {
        return new ResponseEntity<>(employeeService.getEmployee(id), HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee, @PathVariable int id) throws EmployeeException {
        return new ResponseEntity<>(employeeService.createEmployee(employee, id), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> replaceEmployee(@RequestBody Employee employee, @PathVariable int id) throws EmployeeException {
        return new ResponseEntity<>(employeeService.replaceEmployee(employee, id), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee, @PathVariable int id) throws EmployeeException {
        //responseEmployee will evaluate to null if there is no change between input & database
        Employee responseEmployee = employeeService.updateEmployee(employee, id);
        return new ResponseEntity<>(responseEmployee, responseEmployee == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
    }

    @PatchMapping("/{id}/devices/add")
    public ResponseEntity<Employee> addEmployeeDevice(@RequestBody EmployeeDeviceDto devices, @PathVariable int id) throws EmployeeException {
        //responseEmployee will evaluate to null if there is no change between input & database
        Employee responseEmployee = employeeService.addEmployeeDevice(devices, id);
        return new ResponseEntity<>(responseEmployee, responseEmployee == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
    }

    @PatchMapping("/{id}/devices/remove")
    public ResponseEntity<Employee> removeEmployeeDevice(@RequestBody EmployeeDeviceDto devices, @PathVariable int id) throws EmployeeException {
        //responseEmployee will evaluate to null if there is no change between input & database
        Employee responseEmployee = employeeService.removeEmployeeDevice(devices, id);
        return new ResponseEntity<>(responseEmployee, responseEmployee == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable int id) throws EmployeeException {
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/devices/summary")
    public ResponseEntity<List<TotalEmployeeDevicesDto>> getTotalDevices (){
        return new ResponseEntity<>(employeeService.getTotalEmployeeDevices(), HttpStatus.OK);
    }

    @GetMapping("/new")
    public ResponseEntity<List<NewEmployeesDto>> getNewEmployees (){

        return new ResponseEntity<>(employeeService.getNewEmployees(), HttpStatus.OK);
    }

    @GetMapping("/birthdays")
    public ResponseEntity<List<FullTimeEmployeeBirthdayDto>> getFullTimeEmployeeBirthdays (){

        return new ResponseEntity<>(employeeService.getFullTimeEmployeeBirthdays(), HttpStatus.OK);
    }
}
