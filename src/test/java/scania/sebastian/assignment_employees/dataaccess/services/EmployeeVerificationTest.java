package scania.sebastian.assignment_employees.dataaccess.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeRepository;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeSeedData;
import scania.sebastian.assignment_employees.exceptionhandler.*;
import scania.sebastian.assignment_employees.models.domain.Employee;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeVerificationTest {
    EmployeeVerification employeeVerification;
    EmployeeRepository employeeRepository;
    EmployeeSeedData employeeSeedData;
    Employee firstEmployee;

    @BeforeEach
    void init(){
        employeeVerification = new EmployeeVerification();
        employeeRepository = new EmployeeRepository();
        employeeSeedData = new EmployeeSeedData();

        firstEmployee = new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices());
    }

    @Test
    void verifyEmployeeId_validId_dontThrowException() {
        int testId = 1;
        assertDoesNotThrow(() -> employeeVerification.verifyEmployeeId(firstEmployee.getId(), testId));
    }

    @Test
    void verifyEmployeeId_invalidId_throwInvalidEmployeeIdException() {
        int testId = 5;
        assertThrows(InvalidEmployeeObjectException.class, () -> employeeVerification.verifyEmployeeId(firstEmployee.getId(), testId));
    }

    @Test
    void verifyEmployeeInDatabase_employeeExistInDatabase_dontThrowException()  {
        int testId = 1;
        assertDoesNotThrow(() -> employeeVerification.verifyEmployeeInDatabase(testId, employeeRepository.getEmployees()));
    }

    @Test
    void verifyEmployeeInDatabase_employeeDoesntExistInDatabase_throwEmployeeDoesNotExistException()  {
        int testId = 5;
        assertThrows(EmployeeDoesNotExistException.class, () -> employeeVerification.verifyEmployeeInDatabase(testId, employeeRepository.getEmployees()));
    }

    @Test
    void verifyEmployeeNotInDatabase_employeeDoesntExistInDatabase_dontThrowException() {
        int testId = 5;
        assertDoesNotThrow(() -> employeeVerification.verifyEmployeeNotInDatabase(testId, employeeRepository.getEmployees()));
    }

    @Test
    void verifyEmployeeNotInDatabase_employeeExistInDatabase_throwEmployeeAlreadyExistException() {
        int testId = 1;
        assertThrows(EmployeeAlreadyExistException.class, () -> employeeVerification.verifyEmployeeNotInDatabase(testId, employeeRepository.getEmployees()));
    }

    @Test
    void verifyEmployeeObject_employeeValidObject_dontThrowException() {
        assertDoesNotThrow(() -> employeeVerification.verifyEmployeeObject(firstEmployee));
    }

    @Test
    void verifyEmployeeObject_employeeInvalidObject_throwInvalidEmployeeObjectException() {
        firstEmployee.setLastName(null);
        assertThrows(InvalidEmployeeObjectException.class, () -> employeeVerification.verifyEmployeeObject(firstEmployee));
    }

    @Test
    void verifyDeviceNotNull_validDevice_dontThrowException(){
        String testDevice = "Laptop";
        assertDoesNotThrow(() -> employeeVerification.verifyDeviceNotNull(testDevice));
    }

    @Test
    void verifyDeviceNotNull_deviceIsNull_throwInvalidDeviceException(){
        assertThrows(InvalidDeviceException.class, () -> employeeVerification.verifyDeviceNotNull(null));
    }

    @Test
    void devicesEmpty_deviceIsEmpty_returnTrue() {
        boolean expected = true;
        firstEmployee.getDevices().remove(0);
        boolean actual = employeeVerification.devicesEmpty(firstEmployee.getDevices());
        assertEquals(expected, actual);
    }

    @Test
    void devicesEmpty_deviceIsNotEmpty_returnFalse() {
        boolean expected = false;
        boolean actual = employeeVerification.devicesEmpty(firstEmployee.getDevices());
        assertEquals(expected, actual);
    }

    @Test
    void validateEmployeeCodeIsUnique_uniqueEmployeeCode_returnNothing(){
        long uniqueEmployeeCode = 6836048220L;
        int testId = 1;

        assertDoesNotThrow(() -> employeeVerification.validateEmployeeCodeIsUnique(
                testId,
                employeeRepository.getEmployees(),
                uniqueEmployeeCode));
    }
    @Test
    void validateEmployeeCodeIsUnique_existingEmployeeCodeAtCurrentId_returnNothing(){
        long uniqueEmployeeCode = 1634751505L;
        int testId = 2;

        assertDoesNotThrow(() -> employeeVerification.validateEmployeeCodeIsUnique(
                testId,
                employeeRepository.getEmployees(),
                uniqueEmployeeCode));
    }

    @Test
    void validateEmployeeCodeIsUnique_existingEmployeeCode_throwEmployeeAlreadyExistException(){
        long uniqueEmployeeCode = 2018269734L;
        int testId = 2;

        assertThrows(EmployeeAlreadyExistException.class, () -> employeeVerification.validateEmployeeCodeIsUnique(
                testId,
                employeeRepository.getEmployees(),
                uniqueEmployeeCode));
    }

    @Test
    void verifyEmployeeCode_lengthIsTenAndValidLuhn_dontThrowException() {
        long testNumber = 8106014239L;
        assertDoesNotThrow(() -> employeeVerification.verifyEmployeeCode(testNumber));
    }

    @Test
    void verifyEmployeeCode_lengthIsNotTen_throwInvalidEmployeeObjectException() {
        long testNumber = 81060142391L;
        assertThrows(InvalidEmployeeObjectException.class, () -> employeeVerification.verifyEmployeeCode(testNumber));
    }

    @Test
    void verifyEmployeeCode_invalidLuhnCode_throwInvalidEmployeeObjectException() {
        long testNumber = 8106014238L;
        assertThrows(InvalidEmployeeObjectException.class, () -> employeeVerification.verifyEmployeeCode(testNumber));
    }

    @Test
    void verifyEmployeeCodeLength_tenDigitEmployeeCode_returnsTrue(){
        long testNumber = 1234567890L;
        boolean expected = true;
        EmployeeVerification employeeVerification = new EmployeeVerification();

        boolean actual = employeeVerification.validateEmployeeCodeLength(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void verifyEmployeeCodeLength_notTenDigitEmployeeCode_returnsFalse(){
        long testNumber = 123456789L;
        boolean expected = false;
        EmployeeVerification employeeVerification = new EmployeeVerification();

        boolean actual = employeeVerification.validateEmployeeCodeLength(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void validateEmployeeCodeLuhn_validLuhnCode_returnsTrue(){
        long testNumber = 8106014239L;
        boolean expected = true;
        EmployeeVerification employeeVerification = new EmployeeVerification();

        boolean actual = employeeVerification.validateEmployeeCodeLuhn(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void validateEmployeeCodeLuhn_invalidLuhnCode_returnsFalse(){
        long testNumber = 8106014238L;
        boolean expected = false;
        EmployeeVerification employeeVerification = new EmployeeVerification();

        boolean actual = employeeVerification.validateEmployeeCodeLuhn(testNumber);

        assertEquals(expected, actual);
    }

}