package scania.sebastian.assignment_employees.dataaccess.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeRepository;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeSeedData;
import scania.sebastian.assignment_employees.exceptionhandler.*;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.EmployeeDeviceDto;
import scania.sebastian.assignment_employees.models.maps.FullTimeEmployeeBirthdayDtoMapper;
import scania.sebastian.assignment_employees.models.maps.NewEmployeesDtoMapper;
import scania.sebastian.assignment_employees.models.maps.TotalDevicesDtoMapper;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeServiceTest {
    EmployeeService employeeService;
    EmployeeSeedData employeeSeedData;
    Employee firstEmployee;
    ArrayList<String> testDevices;
    EmployeeDeviceDto employeeDeviceDto;

    @BeforeEach
    void init(){
        employeeService = new EmployeeService(
                new EmployeeRepository(),
                new EmployeeVerification(),
                new FullTimeEmployeeBirthdayDtoMapper(),
                new NewEmployeesDtoMapper(),
                new TotalDevicesDtoMapper());
        employeeSeedData = new EmployeeSeedData();

        testDevices = new ArrayList<>();
        testDevices.add("Dell XPS");
        employeeDeviceDto = new EmployeeDeviceDto(1, testDevices);

        firstEmployee = new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices());
    }

    @Test
    void getAllEmployees_callMethod_returnListOfSizeThree() {
        int expected = 3;

        int actual = employeeService.getAllEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void getEmployee_validId_returnEmployee() throws EmployeeException {
        int testData = 1;
        Employee expected = firstEmployee;

        Employee actual = employeeService.getEmployee(testData);

        assertEquals(expected, actual);
    }

    @Test
    void getEmployee_invalidId_throwEmployeeDoesNotExistException() {
        int testId = 5;

        assertThrows(EmployeeDoesNotExistException.class, () -> employeeService.getEmployee(testId));
    }

    @Test
    void createEmployee_matchingIdAndObject_returnEmployee() throws EmployeeException {
        int testId = 5;
        firstEmployee.setEmployeeCode(6666425738L);
        Employee expected = firstEmployee;
        expected.setId(5);

        Employee actual = employeeService.createEmployee(firstEmployee, testId);

        assertEquals(expected, actual);
    }

    @Test
    void createEmployee_mismatchingIdAndObject_returnInvalidEmployeeObjectException() {
        int testId = 5;
        firstEmployee.setId(2);

        assertThrows(InvalidEmployeeObjectException.class, () -> employeeService.createEmployee(firstEmployee, testId));
    }

    @Test
    void createEmployee_employeeIdAlreadyInDatabase_returnEmployeeAlreadyExistException() {
        int testId = 2;
        firstEmployee.setId(2);

        assertThrows(EmployeeAlreadyExistException.class, () -> employeeService.createEmployee(firstEmployee, testId));
    }

    @Test
    void createEmployee_employeeInvalidObject_returnInvalidEmployeeObjectException() {
        int testId = 5;
        Employee invalidEmployee = new Employee(5, null, null, null, 0, null, null, null, null);

        assertThrows(InvalidEmployeeObjectException.class, () -> employeeService.createEmployee(invalidEmployee, testId));
    }

    @Test
    void replaceEmployee_matchingIdAndObject_returnEmployee() throws EmployeeException {
        int testId = 2;
        firstEmployee.setEmployeeCode(6666425738L);
        Employee expected = firstEmployee;
        expected.setId(2);

        Employee actual = employeeService.replaceEmployee(firstEmployee, testId);

        assertEquals(expected, actual);
    }

    @Test
    void replaceEmployee_employeeWithNewId_returnEmployee() throws EmployeeException {
        int testId = 4;
        firstEmployee.setEmployeeCode(6666425738L);
        Employee expected = firstEmployee;
        expected.setId(4);

        Employee actual = employeeService.replaceEmployee(firstEmployee, testId);

        assertEquals(expected, actual);
    }

    @Test
    void replaceEmployee_mismatchingIdAndObject_returnInvalidEmployeeObjectException() {
        int testId = 1;
        firstEmployee.setId(2);

        assertThrows(InvalidEmployeeObjectException.class, () -> employeeService.replaceEmployee(firstEmployee, testId));
    }

    @Test
    void replaceEmployee_employeeInvalidObject_returnInvalidEmployeeObjectException() {
        int testId = 5;
        Employee invalidEmployee = new Employee(5, null, null, null, 0, null, null, null, null);

        assertThrows(InvalidEmployeeObjectException.class, () -> employeeService.replaceEmployee(invalidEmployee, testId));
    }

    @Test
    void updateEmployee_matchingIdAndObject_returnEmployee() throws EmployeeException {
        int testId = 2;
        firstEmployee.setEmployeeCode(6666425738L);
        Employee expected = firstEmployee;
        expected.setId(2);

        Employee actual = employeeService.updateEmployee(firstEmployee, testId);

        assertEquals(expected, actual);
    }

    @Test
    void updateEmployee_employeeWithNewId_returnEmployeeDoesNotExistException() {
        int testId = 4;
        Employee expected = firstEmployee;
        expected.setId(4);

        assertThrows(EmployeeDoesNotExistException.class, () -> employeeService.updateEmployee(firstEmployee, testId));
    }

    @Test
    void updateEmployee_mismatchingIdAndObject_returnInvalidEmployeeObjectException() {
        int testId = 1;
        firstEmployee.setId(2);

        assertThrows(InvalidEmployeeObjectException.class, () -> employeeService.updateEmployee(firstEmployee, testId));
    }

    @Test
    void updateEmployee_employeeInvalidObject_returnEmployeeDoesNotExistException() {
        int testId = 5;
        Employee invalidEmployee = new Employee(5, null, null, null, 0, null, null, null, null);

        assertThrows(EmployeeDoesNotExistException.class, () -> employeeService.updateEmployee(invalidEmployee, testId));
    }

    @Test
    void deleteEmployee_deleteValidEmployee_employeeListSizeIsTwo() throws EmployeeException {
        int testId = 1;
        int expected = 2;

        employeeService.deleteEmployee(testId);
        int actual = employeeService.getWorkersRepository().getEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void deleteEmployee_deleteInvalidEmployee_return() {
        int testId = 5;

        assertThrows(EmployeeDoesNotExistException.class, () -> employeeService.deleteEmployee(testId));
    }

    @Test
    void addEmployeeDevice_validEmployeeId_returnEmployee() throws EmployeeException {
        int testId = 1;
        String testDevice = "Dell XPS";
        Employee expected = firstEmployee;
        expected.getDevices().add(testDevice);

        Employee actual = employeeService.addEmployeeDevice(employeeDeviceDto, testId);

        assertEquals(expected, actual);
    }

    @Test
    void addEmployeeDevice_invalidEmployeeId_returnException() {
        int testId = 5;

        assertThrows(EmployeeDoesNotExistException.class, () -> employeeService.addEmployeeDevice(employeeDeviceDto, testId));
    }

    @Test
    void addEmployeeDevice_deviceIsNull_returnInvalidDeviceException() {
        int testId = 2;
        testDevices.remove(0);
        testDevices.add(null);
        EmployeeDeviceDto testDevice = new EmployeeDeviceDto(2, testDevices);

        assertThrows(InvalidDeviceException.class, () -> employeeService.addEmployeeDevice(testDevice, testId));
    }

    @Test
    void addEmployeeDevice_devicesIsNull_returnInvalidDeviceException() {
        int testId = 2;
        EmployeeDeviceDto testDevice = new EmployeeDeviceDto(2, null);

        assertThrows(InvalidDeviceException.class, () -> employeeService.addEmployeeDevice(testDevice, testId));
    }

    @Test
    void removeEmployeeDevice_validEmployeeId_returnEmployee() throws EmployeeException {
        int testId = 1;
        Employee expected = firstEmployee;
        expected.getDevices().remove(testDevices.get(0));

        Employee actual = employeeService.removeEmployeeDevice(employeeDeviceDto, testId);

        assertEquals(expected, actual);
    }

    @Test
    void removeEmployeeDevice_invalidEmployeeId_returnException() {
        int testId = 5;

        assertThrows(EmployeeDoesNotExistException.class, () -> employeeService.removeEmployeeDevice(employeeDeviceDto, testId));
    }

    @Test
    void removeEmployeeDevice_deviceIsNull_returnInvalidDeviceException() {
        int testId = 2;
        testDevices.remove(0);
        testDevices.add(null);
        EmployeeDeviceDto testDevice = new EmployeeDeviceDto(2, testDevices);

        assertThrows(InvalidDeviceException.class, () -> employeeService.removeEmployeeDevice(testDevice, testId));
    }

    @Test
    void removeEmployeeDevice_devicesIsNull_returnInvalidDeviceException() {
        int testId = 2;
        EmployeeDeviceDto testDevice = new EmployeeDeviceDto(2, null);

        assertThrows(InvalidDeviceException.class, () -> employeeService.removeEmployeeDevice(testDevice, testId));
    }

    @Test
    void getTotalEmployeeDevices_completeList_returnListSizeThree() {
        int expected =3;

        int actual = employeeService.getTotalEmployeeDevices().size();

        assertEquals(expected, actual);
    }

    //affected with expected 2, testIndex 1
    @Test
    void getTotalEmployeeDevices_deviceAmountFromSecondEmployee_returnSizeTwo() {
        int expected = 1;
        int testIndex = 0;

        int actual = employeeService.getTotalEmployeeDevices().get(testIndex).getTotalDevices();

        assertEquals(expected, actual);
    }

    @Test
    void getTotalEmployeeDevices_fullNameOfFirstEmployeeInList_returnFullName() {
        String expected = "John Doe";
        int testIndex = 0;

        String actual = employeeService.getTotalEmployeeDevices().get(testIndex).getFullName();

        assertEquals(expected, actual);
    }

    @Test
    void getNewEmployees_completeList_returnListSizeOne() {
        int expected =1;

        int actual = employeeService.getNewEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void getNewEmployees_getFullName_returnFullName() {
        String expected = "John Doe";
        int testIndex = 0;

        String actual = employeeService.getNewEmployees().get(testIndex).getFullName();

        assertEquals(expected, actual);
    }

    @Test
    void getNewEmployees_getEmployeeCode_returnEmployeeCode() {
        long expected = 2018269734;
        int testIndex = 0;

        long actual = employeeService.getNewEmployees().get(testIndex).getEmployeeCode();

        assertEquals(expected, actual);
    }

    @Test
    void getFullTimeEmployeeBirthdays_completeList_returnListSizeTwo() {
        int expected = 2;

        int actual = employeeService.getFullTimeEmployeeBirthdays().size();

        assertEquals(expected, actual);
    }

    @Test
    void getFullTimeEmployeeBirthdays_secondEmployeeDateOfBirth_returnDateOfBirth() {
        LocalDate expected = LocalDate.of(1987,3,22);
        int testIndex = 1;

        LocalDate actual = employeeService.getFullTimeEmployeeBirthdays().get(testIndex).getDateOfBirth();

        assertEquals(expected, actual);
    }

    @Test
    void getFullTimeEmployeeBirthdays_daysUntilBirthday_returnBetweenZeroAndThreeHundredSixtyFive() {
        int testIndex = 1;
        int actual = employeeService.getFullTimeEmployeeBirthdays().get(testIndex).getDaysUntilBirthday();

        boolean expected = (actual >= 0 && actual <= 365);

        assertTrue(expected);
    }

}