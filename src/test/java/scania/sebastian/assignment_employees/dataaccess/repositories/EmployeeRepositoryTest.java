package scania.sebastian.assignment_employees.dataaccess.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scania.sebastian.assignment_employees.models.domain.Employee;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {
    EmployeeRepository employeeRepository;
    EmployeeSeedData employeeSeedData;
    Employee firstEmployee;

    @BeforeEach
    void init(){
        employeeRepository = new EmployeeRepository();
        employeeSeedData = new EmployeeSeedData();

        firstEmployee = new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices());
    }

    @Test
    void getAllEmployees_callMethod_returnListOfSizeThree() {
        int expected = 3;
        int actual = employeeRepository.getAllEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void getEmployee_validId_returnEmployee() {
        int testId = 1;
        Employee expected = firstEmployee;
        Employee actual = employeeRepository.getEmployee(testId);

        assertEquals(expected, actual);
    }

    @Test
    void getEmployee_invalidId_returnNull() {
        int testId = 4;
        Employee actual = employeeRepository.getEmployee(testId);

        assertNull(actual);
    }

    @Test
    void createEmployee_validEmployee_returnEmployee() {
        Employee expected = firstEmployee;
        Employee actual = employeeRepository.createEmployee(firstEmployee);

        assertEquals(expected, actual);
    }

    @Test
    void createEmployee_validEmployee_returnEmployeeListSizeFour() {
        int expected = 4;
        firstEmployee.setId(4);
        employeeRepository.createEmployee(firstEmployee);
        int actual = employeeRepository.getEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void replaceEmployee_updateExistingEmployee_returnNewEmployeeLastName() {
        String testData = "Smith";
        firstEmployee.setLastName(testData);
        String expected = firstEmployee.getLastName();

        String actual = employeeRepository.replaceEmployee(firstEmployee).getLastName();

        assertEquals(expected, actual);
    }

    @Test
    void replaceEmployee_createNewEmployee_returnEmployeeListSizeFour() {
        int expected = 4;
        firstEmployee.setId(4);
        employeeRepository.replaceEmployee(firstEmployee);
        int actual = employeeRepository.getEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void updateEmployee_updateExistingEmployee_returnNewEmployeeLastName() {
        String testData = "Smith";
        firstEmployee.setLastName(testData);
        String expected = firstEmployee.getLastName();
        String actual = employeeRepository.updateEmployee(firstEmployee).getLastName();

        assertEquals(expected, actual);
    }

    @Test
    void deleteEmployee_validId_returnListSizeIsTwo() {
        int testId = 1;
        int expected = 2;
        employeeRepository.deleteEmployee(testId);
        int actual = employeeRepository.getEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void deleteEmployee_invalidId_returnListSizeIsThree() {
        int testId = 4;
        int expected = 3;
        employeeRepository.deleteEmployee(testId);
        int actual = employeeRepository.getEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void addEmployeeDevice_validEmployee_returnDeviceSizeThree() {
        String testDevice = "Laptop";
        int employeeId = 2;
        int expected = 3;

        employeeRepository.addEmployeeDevice(testDevice, employeeId);
        int actual = employeeRepository.getEmployee(employeeId).getDevices().size();

        assertEquals(expected, actual);
    }

    @Test
    void addEmployeeDevice_validEmployee_returnEmployee() {
        String testDevice = "Laptop";
        int employeeId = 1;
        Employee expected = firstEmployee;
        expected.getDevices().add(testDevice);

        Employee actual = employeeRepository.addEmployeeDevice(testDevice, employeeId);

        assertEquals(expected, actual);
    }

    @Test
    void addEmployeeDevice_invalidEmployee_throwNullPointerException() {
        String testDevice = "Laptop";
        int employeeId = 5;

        assertThrows(NullPointerException.class, () -> employeeRepository.addEmployeeDevice(testDevice, employeeId));
    }

    @Test
    void removeEmployeeDevice_validEmployeeAndValidDevice_returnDeviceSizeZero() {
        String testDevice = "Dell XPS 13";
        int employeeId = 1;
        int expected = 0;

        employeeRepository.removeEmployeeDevice(testDevice, employeeId);
        int actual = employeeRepository.getEmployee(employeeId).getDevices().size();

        assertEquals(expected, actual);
    }

    @Test
    void removeEmployeeDevice_validEmployeeAndValidDevice_returnEmployee() {
        String testDevice = "Dell XPS 13";
        int employeeId = 1;
        Employee expected = firstEmployee;
        expected.getDevices().remove(testDevice);

        Employee actual = employeeRepository.removeEmployeeDevice(testDevice, employeeId);

        assertEquals(expected, actual);
    }

    @Test
    void removeEmployeeDevice_invalidEmployee_throwNullPointerException() {
        String testDevice = "Dell XPS 13";
        int employeeId = 4;

        assertThrows(NullPointerException.class, () -> employeeRepository.removeEmployeeDevice(testDevice, employeeId));
    }

    @Test
    void removeEmployeeDevice_invalidDevice_returnDeviceSizeTwo() {
        String testDevice = "invalid device";
        int employeeId = 2;
        int expected = 2;

        employeeRepository.removeEmployeeDevice(testDevice, employeeId);
        int actual = employeeRepository.getEmployee(employeeId).getDevices().size();

        assertEquals(expected, actual);
    }

}