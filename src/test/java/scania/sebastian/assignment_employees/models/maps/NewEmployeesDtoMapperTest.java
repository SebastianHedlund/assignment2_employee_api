package scania.sebastian.assignment_employees.models.maps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeSeedData;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.NewEmployeesDto;

import static org.junit.jupiter.api.Assertions.*;

class NewEmployeesDtoMapperTest {
    NewEmployeesDtoMapper newEmployeesDtoMapper;
    EmployeeSeedData employeeSeedData;
    Employee firstEmployee;

    @BeforeEach
    void init(){
        newEmployeesDtoMapper = new NewEmployeesDtoMapper();
        employeeSeedData = new EmployeeSeedData();
        firstEmployee = new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices());
    }

    @Test
    void mapNewEmployeesDto_createNewEmployeesDto_returnCorrectDto() {
        NewEmployeesDto expected = new NewEmployeesDto(2018269734, "John Doe");
        NewEmployeesDto actual = newEmployeesDtoMapper.mapNewEmployeesDto(firstEmployee);

        assertEquals(expected, actual);
    }
}