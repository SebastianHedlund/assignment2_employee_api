package scania.sebastian.assignment_employees.models.maps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeSeedData;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.FullTimeEmployeeBirthdayDto;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class FullTimeEmployeeBirthdayDtoMapperTest {
    FullTimeEmployeeBirthdayDtoMapper fullTimeEmployeeBirthdayDtoMapper;
    EmployeeSeedData employeeSeedData;
    Employee firstEmployee;

    @BeforeEach
    void init(){
        fullTimeEmployeeBirthdayDtoMapper = new FullTimeEmployeeBirthdayDtoMapper();
        employeeSeedData = new EmployeeSeedData();
        firstEmployee = new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices());
    }

    @Test
    void mapFullTimeEmployeeDto_createFullTimeEmployeeBirthdayDto_returnCorrectDto() {
        FullTimeEmployeeBirthdayDto expected = new FullTimeEmployeeBirthdayDto(
                "John Doe",
                LocalDate.of(1980, 2,14),
                100);
        FullTimeEmployeeBirthdayDto actual = fullTimeEmployeeBirthdayDtoMapper.mapFullTimeEmployeeDto(firstEmployee, 100);

        assertEquals(expected, actual);

    }

}