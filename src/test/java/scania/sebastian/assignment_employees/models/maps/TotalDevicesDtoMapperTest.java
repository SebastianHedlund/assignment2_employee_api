package scania.sebastian.assignment_employees.models.maps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scania.sebastian.assignment_employees.dataaccess.repositories.EmployeeSeedData;
import scania.sebastian.assignment_employees.models.domain.Employee;
import scania.sebastian.assignment_employees.models.dto.TotalEmployeeDevicesDto;

import static org.junit.jupiter.api.Assertions.*;

class TotalDevicesDtoMapperTest {
    TotalDevicesDtoMapper totalDevicesDtoMapper;
    EmployeeSeedData employeeSeedData;
    Employee firstEmployee;

    @BeforeEach
    void init(){
        totalDevicesDtoMapper = new TotalDevicesDtoMapper();
        employeeSeedData = new EmployeeSeedData();
        firstEmployee = new Employee(
                employeeSeedData.getFirstEmployeeId(),
                employeeSeedData.getFirstEmployeeFirstName(),
                employeeSeedData.getFirstEmployeeLastName(),
                employeeSeedData.getFirstEmployeeDateOfBirth(),
                employeeSeedData.getFirstEmployeeCode(),
                employeeSeedData.getFirstEmployeeDateHired(),
                employeeSeedData.getFirstEmployeePosition(),
                employeeSeedData.getFirstEmployeeWorkFullTime(),
                employeeSeedData.getFirstEmployeeDevices());
    }

    @Test
    void mapTotalEmployeeDevicesDto_createTotalDevicesDto_returnCorrectDto() {
        TotalEmployeeDevicesDto expected = new TotalEmployeeDevicesDto("John Doe", 1);
        TotalEmployeeDevicesDto actual = totalDevicesDtoMapper.mapTotalEmployeeDevicesDto(firstEmployee);

        assertEquals(expected, actual);
    }
}