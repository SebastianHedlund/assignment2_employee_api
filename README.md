# Assignment2_Employee_API

Assignment2_Employee_API is a web application written in Java using Spring. 
The API store employees in memory and allow the client to perform different operations for these employees. 
Three different employees are generated during application launch to be used as mock data.

## Installation

Download the repository. Using maven, build the package with following command

```bash
mvn package
```

## Usage

From the projects root folder, start the application using following command
```bash
java -jar target/assignment_employees-0.0.1-SNAPSHOT.jar
```

Using postman, or other API platform you can request following API endpoints on address http://localhost:8080/employees

**_NOTE:_** {id} refers to a parameter, this can be any whole number.

- GET / - Returns information about all employees in the database.
- GET /{id} - Return information about a specific employee
- GET /devices/summary - Return each employee's full name and how many devices they have.
- GET /new - Return employee code & full name of all employee's hired within the last two years.
- GET /birthdays - Return all full time employees full name, date of brith and amount of days until their birthday.


- POST /{id} - Requires JSON body in following format. It will add the provided employee to the database, id in URI & JSON object must be the same. Id & employeeCode can not already be taken.
```JSON
{
  "id": 4,
  "firstName": "Sebastian",
  "lastName": "Hedlund",
  "dateOfBirth": "1992-10-16",
  "employeeCode": 1634751505,
  "dateHired": "2022-02-01",
  "position": "DEVELOPER",
  "employmentHours" : "FULLTIME",
  "devices" : ["Laptop"]
}
```
- PUT /{id} - Behave the same as post, but will overwrite any previous stored employee with same id
- PATCH /{id} - Requires JSON body with any amount of fields to update. id must always be provided. Example to update firstName and lastName on id 4:
```JSON
{
"id": 4,
"firstName": "Ellen",
"lastName": "Hallberg"
}
```
- PATCH /{id}/devices/add - Adds a device to the employee (correlated with the provided id), requires a JSON body in following format:
```JSON
  {
  "id" : 2,
  "devices": ["Monitor"]
  }
```
- PATCH /{id}/devices/remove - Same behaviour and requirement as devices/add. Except this endpoint remove a device instead.
- DEL /{id} - Delete an employee (correlated with the provided id) from the database.

It's also possible to use the public API available to use with this URL: https://sebastian-employee-assignment.herokuapp.com/employees

**_NOTE:_** This public API is available to anyone, the data stored may be ridiculous :)

## Feedback
Please provide any feedback for improvements!
